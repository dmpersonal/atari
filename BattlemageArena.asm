;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Battlemage Arena
;
;	First learning project for 6502 assembly and
; Atari 2600 development.
;
; Project Start: 23-03-2016
;
;	Releases:
;		v1.0 - ????
;	
;
; Author: Diogo Muller 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	PROCESSOR 6502

	INCLUDE "Lib/vcs.h"
	INCLUDE "Lib/macro.h"


;-------------------------------------------------------------------------------
;
;		DEFINITIONS
;		
;-------------------------------------------------------------------------------

; Video Mode

VIDEO_MODE_NTSC 		= 	0
VIDEO_MODE_PAL 		= 	1
VIDEO_MODE_SECAM 	= 	2

VIDEO_MODE = VIDEO_MODE_NTSC

;-------------------------------------------------------------------------------
;
;		CONSTANTS
;		
;-------------------------------------------------------------------------------

#if VIDEO_MODE = VIDEO_MODE_NTSC
CONST_VBLANK_SCANLINES	= 	#37 		;  37 NTSC,  45 PAL and SECAM.
CONST_SCREEN_SCANLINES	= 	#192 	; 192 NTSC, 228 PAL and SECAM.
CONST_OVERSC_SCANLINES	= 	#30		;  30 NTSC,  36 PAL and SECAM.
	
CONST_BKGD_COLOR			= 	$90 		; $90 NTSC, $B0 PAL, $00 SECAM.
CONST_PLFL_COLOR			= 	$50 		; $50 NTSC, $80 PAL, $0E SECAM.
CONST_PLA1_COLOR			= 	$34 		; $34 NTSC, $44 PAL, $04 SECAM.
CONST_PLA2_COLOR 			= 	$72 		; $72 NTSC, $C2 PAL, $02 SECAM.
#endif

#if VIDEO_MODE = VIDEO_MODE_PAL
CONST_VBLANK_SCANLINES	= 	#45 		;  37 NTSC,  45 PAL and SECAM.
CONST_SCREEN_SCANLINES	= 	#228		; 192 NTSC, 228 PAL and SECAM.
CONST_OVERSC_SCANLINES	= 	#36		;  30 NTSC,  36 PAL and SECAM.
		
CONST_BKGD_COLOR			= 	$B0 		; $90 NTSC, $B0 PAL, $00 SECAM.
CONST_PLFL_COLOR			= 	$80 		; $50 NTSC, $80 PAL, $0E SECAM.
CONST_PLA1_COLOR			= 	$44 		; $34 NTSC, $44 PAL, $04 SECAM.
CONST_PLA2_COLOR 			= 	$C2 		; $72 NTSC, $C2 PAL, $02 SECAM.
#endif	

#if VIDEO_MODE = VIDEO_MODE_SECAM
CONST_VBLANK_SCANLINES	= 	#45 		;  37 NTSC,  45 PAL and SECAM.
CONST_SCREEN_SCANLINES	= 	#228		; 192 NTSC, 228 PAL and SECAM.
CONST_OVERSC_SCANLINES	= 	#36		;  30 NTSC,  36 PAL and SECAM.
		
CONST_BKGD_COLOR			= 	$00 		; $90 NTSC, $B0 PAL, $00 SECAM.
CONST_PLFL_COLOR			= 	$0E 		; $50 NTSC, $80 PAL, $0E SECAM.
CONST_PLA1_COLOR			= 	$04 		; $34 NTSC, $44 PAL, $04 SECAM.
CONST_PLA2_COLOR 			= 	$02 		; $72 NTSC, $C2 PAL, $02 SECAM.
#endif

CONST_WALL_SCANLINES 	= #8													; Playfield Wall Size.
CONST_PLAY_SCANLINES 	= #CONST_SCREEN_SCANLINES - #8		; Playfield Size.
	
;-------------------------------------------------------------------------------
;
;		VARIABLES
;		
;-------------------------------------------------------------------------------	

		SEG.U Variables
		ORG $0080 ; Start of RAM.
		
VAR_TEST 			DS 1			; Test Variable

;-------------------------------------------------------------------------------
;
;		MAIN CODE.
;		
;-------------------------------------------------------------------------------

		SEG
		ORG $F000
	
; Initializes the game, Cleaning TIA and RAM.
Reset:
		LDX #0
		LDA #0

; Clear TIA (00-7F) and RAM (80-FF)		
Clear:
		STA #0,X
		INX
		BNE Clear

		;JMP Game
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; TITLE SCREEN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
RunTitle:		
		LDX #0  ; line #
		
		; Background Color
		LDA #$9D
		STA COLUBK
		
		; Playfield Color
		LDA #$90
		STA COLUPF
		
StartOfTitleFrame:
		; START OF VBLANK PROCESSING
		LDA #0
		STA VBLANK
		
		LDA #2
		STA VSYNC

		; First 3 Scanlines of VSync Signal.
		STA WSYNC
		STA WSYNC
		STA WSYNC

		LDA #0
		STA VSYNC           

		;------------------------------------------------
		; VBLANK
		LDX #0
		
RunTitleVerticalBlank:
		STA WSYNC
		INX
		CPX #CONST_VBLANK_SCANLINES
		BNE RunTitleVerticalBlank
		
		
		LDX #CONST_SCREEN_SCANLINES
		
DrawTitleLine:

		; Set the first half of the screen's playfield.
		LDA IMG_TITLE_STRIP_0,x	; PF0 left
		STA PF0
		LDA IMG_TITLE_STRIP_1,x	; PF1 left
		STA PF1
		LDA IMG_TITLE_STRIP_2,x	; PF2 left
		STA PF2

		; Delay until middle of the screen.
TitleDelay:
		NOP

		; Set the second half of the screen's playfield.
		LDA IMG_TITLE_STRIP_3,x	; PF0 right
		STA PF0
		LDA IMG_TITLE_STRIP_4,x	; PF1 right
		STA PF1
		LDA IMG_TITLE_STRIP_5,x	; PF2 right
		STA PF2

		; Wait until the line end.
		STA WSYNC
		DEX
		CPX #0

		BNE DrawTitleLine
		
		JMP StartOfTitleFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN GAME
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
RunGame:	
		; Once only game initialization.
		LDA #0
		
		; Background Color
		LDA #CONST_BKGD_COLOR
		STA COLUBK
		
		; Playfield Color
		LDA #CONST_PLFL_COLOR
		STA COLUPF
		
; Initialize CtrlPF
		LDA #%00000001
		STA CTRLPF

; Update/Drawing Called Each Frame.
StartOfFrame:
		; START OF VBLANK PROCESSING
		LDA #0
		STA VBLANK
		
		LDA #2
		STA VSYNC

		; First 3 Scanlines of VSync Signal.
		STA WSYNC
		STA WSYNC
		STA WSYNC

		LDA #0
		STA VSYNC           

		;------------------------------------------------
		; VBLANK
		LDX #0
		
RunVerticalBlank:
		STA WSYNC
		INX
		CPX #CONST_VBLANK_SCANLINES
		BNE RunVerticalBlank


		;------------------------------------------------
		; FRAME
ProcessFrame:		
		
		LDX #0
		
		; Set the Upper Wall.
		LDA #%11111111
		STA PF0
		STA PF1
		STA PF2
		
MakeTopWall:
		STA WSYNC
		INX
		CPX  #CONST_WALL_SCANLINES
		BNE MakeTopWall
		
		; Set the Playfield Walls
		LDA #%00010000
		STA PF0
		LDA #0
		STA PF1
		STA PF2
		
MakePlayfield:
		STA WSYNC
		INX
		CPX #CONST_PLAY_SCANLINES
		BNE MakePlayfield
		
		; Set the Bottom Wall.
		LDA #%11111111
		STA PF0
		STA PF1
		STA PF2
		
MakeBottomWall:
		STA WSYNC
		INX
		CPX  #CONST_SCREEN_SCANLINES
		BNE MakeBottomWall


       ;------------------------------------------------
	   ; END OF SCREEN - BEGIN BLANKING
		LDA #%01000010

		STA VBLANK 
		
		;------------------------------------------------
		; OVERSCAN

		LDX #0
		
RunOverscan:
		STA WSYNC
		INX
		CPX #CONST_OVERSC_SCANLINES
		BNE RunOverscan

		;------------------------------------------------
		; Go to Next Frame
		
		JMP StartOfFrame 


;-------------------------------------------------------------------------------
;
;		CONTENT
;		
;-------------------------------------------------------------------------------		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; TITLE IMAGE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

IMG_TITLE:

IMG_TITLE_STRIP_0:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
IMG_TITLE_STRIP_1:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 247
	.byte 247
	.byte 247
	.byte 244
	.byte 243
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 224
	.byte 224
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 238
	.byte 245
	.byte 245
	.byte 241
	.byte 241
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 252
	.byte 252
	.byte 252
	.byte 252
	.byte 253
	.byte 253
	.byte 253
	.byte 253
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 192
	.byte 192
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 222
	.byte 192
	.byte 192
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 219
	.byte 195
	.byte 195
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
IMG_TITLE_STRIP_2:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 255
	.byte 255
	.byte 255
	.byte 136
	.byte 174
	.byte 173
	.byte 171
	.byte 136
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 69
	.byte 85
	.byte 85
	.byte 85
	.byte 84
	.byte 127
	.byte 255
	.byte 177
	.byte 173
	.byte 173
	.byte 173
	.byte 173
	.byte 173
	.byte 241
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 21
	.byte 21
	.byte 213
	.byte 213
	.byte 217
	.byte 217
	.byte 17
	.byte 17
	.byte 213
	.byte 213
	.byte 213
	.byte 213
	.byte 17
	.byte 17
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 171
	.byte 171
	.byte 171
	.byte 171
	.byte 171
	.byte 171
	.byte 139
	.byte 139
	.byte 171
	.byte 171
	.byte 171
	.byte 171
	.byte 139
	.byte 139
	.byte 251
	.byte 251
	.byte 250
	.byte 250
	.byte 248
	.byte 248
	.byte 249
	.byte 249
	.byte 251
	.byte 251
	.byte 251
	.byte 251
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 181
	.byte 181
	.byte 181
	.byte 181
	.byte 181
	.byte 181
	.byte 177
	.byte 177
	.byte 181
	.byte 181
	.byte 181
	.byte 181
	.byte 17
	.byte 17
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
IMG_TITLE_STRIP_3:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 240
	.byte 240
	.byte 240
	.byte 32
	.byte 160
	.byte 32
	.byte 160
	.byte 32
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 80
	.byte 80
	.byte 80
	.byte 80
	.byte 80
	.byte 208
	.byte 240
	.byte 128
	.byte 160
	.byte 160
	.byte 160
	.byte 160
	.byte 128
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 208
	.byte 208
	.byte 80
	.byte 80
	.byte 80
	.byte 80
	.byte 144
	.byte 144
	.byte 144
	.byte 144
	.byte 208
	.byte 208
	.byte 208
	.byte 208
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 128
	.byte 128
	.byte 160
	.byte 160
	.byte 160
	.byte 160
	.byte 160
	.byte 160
	.byte 224
	.byte 224
	.byte 224
	.byte 224
	.byte 128
	.byte 128
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 176
	.byte 16
	.byte 16
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 240
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
IMG_TITLE_STRIP_4:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 255
	.byte 255
	.byte 255
	.byte 127
	.byte 127
	.byte 127
	.byte 255
	.byte 127
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 47
	.byte 239
	.byte 43
	.byte 163
	.byte 47
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 87
	.byte 87
	.byte 87
	.byte 87
	.byte 87
	.byte 87
	.byte 71
	.byte 71
	.byte 87
	.byte 87
	.byte 87
	.byte 87
	.byte 71
	.byte 71
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 31
	.byte 31
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 31
	.byte 31
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 31
	.byte 31
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 136
	.byte 136
	.byte 187
	.byte 187
	.byte 187
	.byte 187
	.byte 184
	.byte 184
	.byte 187
	.byte 187
	.byte 187
	.byte 187
	.byte 184
	.byte 184
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 255
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
IMG_TITLE_STRIP_5:
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 127
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0

; END


;-------------------------------------------------------------------------------
;
;		MAIN / BOOTSTRAP
;		
;-------------------------------------------------------------------------------
		
		ORG $FFFA
		
InterruptVectors:
		.word Reset          ; NMI
		.word Reset          ; RESET
		.word Reset          ; IRQ
		END

		
