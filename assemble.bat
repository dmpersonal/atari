@ECHO OFF

:main
	IF EXIST Out  (
		echo Clearing Previous Output Directory
		rmdir Out /s /q
	)

	echo Creating Output Directory
	mkdir Out

	echo Assembling %1
	dasm %1 -lOut/%~n1.txt -f3 -v5 -oOut/%~n1.bin > Out/DASM-Output-%~n1.txt

	IF EXIST Out/%~n1.bin (
		echo Running Out/%~n1.bin
		Stella Out/%~n1.bin
	) ELSE (
		echo Error Assembling %1. Please check Out/DASM-Output-%~n1.txt for more info.
	)

	EXIT /B 0

:print
	@ECHO ON
	ECHO %*
	@ECHO OFF
	EXIT /B 0